/***********************************************************************//**
 * @file        config.c
 * @brief       Contains all functions support for CONFIG firmware library on LPC17xx
 * @version     1.0
 * @date        6. October. 2019
 * @author      Monsierra Lucas Gabriel - Lenta Luis
 **************************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
 **********************************************************************/

/* Peripheral group ----------------------------------------------------------- */
/** @addtogroup TP_FINAL
 * @{
 */

/* Includes ------------------------------------------------------------------- */
#include "config.h"
#include "auxfunction.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpdma.h"
#include "lpc17xx_dac.h"
#include "lpc17xx_exti.h"
#include "lpc17xx_adc.h"
#include "lpc17xx_timer.h"
#include "sdcard.h"
#include "config.h"

/* If this source file built with example, the LPC17xx FW library configuration
 * file in each example directory ("lpc17xx_libcfg.h") must be included,
 * otherwise the default FW library configuration file must be included instead
 */
#ifdef __BUILD_WITH_EXAMPLE__
#include "lpc17xx_libcfg.h"
#else
#include "lpc17xx_libcfg_default.h"
#endif /* __BUILD_WITH_EXAMPLE__ */


#ifdef _AUX_FUNC


/* Public Functions ----------------------------------------------------------- */
/** @addtogroup AUX_FUNC_Public_Functions
 * @{
 */

/* AUX_FUNC ------------------------------------------------------------------------------ */

/*********************************************************************//**
 * @brief       Turn on red led (LPC 1769 rev. D)
 * @param 		None
 * @return 		None
 **********************************************************************/
void redLedOn(void)
{
	GPIO_SetValue(PINSEL_PORT_0, (1 << REDLED));
	GPIO_ClearValue(PINSEL_PORT_3, (1 << GREENLED));
}

/*********************************************************************//*
 * @brief    	    Turn on green led
 * @param[in]		None
 * @return 			None
 ***********************************************************************/
void greenLedOn(void)
{
	GPIO_SetValue(PINSEL_PORT_3, (1 << GREENLED));
	GPIO_ClearValue(PINSEL_PORT_0, (1 << REDLED));
}
/*********************************************************************//**
 * @brief       Turn off red led (LPC 1769 rev. D)
 * @param 		None
 * @return 		None
 **********************************************************************/
void redLedOff(void)
{
	GPIO_ClearValue(PINSEL_PORT_0, (1 << REDLED));
}

/*********************************************************************//*
 * @brief    	    Turn off green led
 * @param[in]		None
 * @return 			None
 ***********************************************************************/
void greenLedOff(void)
{
	GPIO_ClearValue(PINSEL_PORT_3, (1 << GREENLED));
}

/*********************************************************************//*
 * @brief    	    Transfer data from Memory to UART
 * @param[in]		None
 * @return 			None
 ***********************************************************************/
void transferMem2Uart(void)
{
	GPDMA_ChannelCmd(0, ENABLE);					// Enable GPDMA Channel 0 (M2Tx)
	GPDMA_ChannelCmd(1, DISABLE);					// Disable GPDMA Channel 1 (M2DAC)
	GPDMA_ChannelCmd(2, DISABLE);					// Disable GPDMA Channel 2 (Rx2M)
	GPDMA_ChannelCmd(3, DISABLE);					// Disable GPDMA Channel 3 (ADC2M)
}

/*********************************************************************//*
 * @brief    	    Transfer data from Memory to DAC
 * @param[in]		None
 * @return 			None
 ***********************************************************************/
void transferMem2Dac(void)
{
	GPDMA_ChannelCmd(0, DISABLE);					// Disable GPDMA Channel 0 (M2Tx)
	GPDMA_ChannelCmd(1, ENABLE);					// Enable GPDMA Channel 1 (M2DAC)
	GPDMA_ChannelCmd(2, DISABLE);					// Disable GPDMA Channel 2 (Rx2M)
	GPDMA_ChannelCmd(3, DISABLE);					// Disable GPDMA Channel 3 (ADC2M)
}

/*********************************************************************//*
 * @brief    	    Transfer data from UART to Memory
 * @param[in]		None
 * @return 			None
 ***********************************************************************/
void transferUART2Mem(void)
{
	GPDMA_ChannelCmd(0, DISABLE);					// Disable GPDMA Channel 0 (M2Tx)
	GPDMA_ChannelCmd(1, DISABLE);					// Disable GPDMA Channel 1 (M2DAC)
	GPDMA_ChannelCmd(2, ENABLE);					// Enable GPDMA Channel 2 (Rx2M)
	GPDMA_ChannelCmd(3, DISABLE);					// Disable GPDMA Channel 3 (ADC2M)
}

/*********************************************************************//*
 * @brief    	    Transfer data from ADC to Memory
 * @param[in]		None
 * @return 			None
 ***********************************************************************/
void transferADC2Mem(void)
{
	GPDMA_ChannelCmd(0, DISABLE);					// Disable GPDMA Channel 0 (M2Tx)
	GPDMA_ChannelCmd(1, DISABLE);					// Disable GPDMA Channel 1 (M2DAC)
	GPDMA_ChannelCmd(2, DISABLE);					// Disable GPDMA Channel 2 (Rx2M)
	GPDMA_ChannelCmd(3, ENABLE);					// Enable GPDMA Channel 3 (ADC2M)
}

/*********************************************************************//*
 * @brief    	    Start record audio
 * @param[in]		None
 * @return 			None
 ***********************************************************************/
void recordAudio(void)
{
	ADC_StartCmd(LPC_ADC, ADC_START_NOW);			// Start conversion
	TIM_Cmd(LPC_TIM0, ENABLE);						// Start count
}

/*********************************************************************//*
 * @brief    	    Read SD Card
 * @param[in]		None
 * @return 			None
 ***********************************************************************/
void readSD(void)
{
	uint8_t i;
	uint8_t tem8;
	uint32_t tem32;
	if (CONFIG_sdConfig() == SD_OK) {
		//Clear receive buffer
		for (i = 1; i < 18; i++) {
			sd_data_buf[i] = 0;
		}

		// Reading SD card's CID register...
		if (SD_GetCID() != SD_OK) { // Fail
			SPI_DeInit(LPC_SPI);
		} else { // Done!
			for (i = 4; i < 9; i++)
			tem8 = (sd_data_buf[9] & 0xF0) >> 4;
			tem8 = (sd_data_buf[9] & 0x0F);
			tem32 = (sd_data_buf[13] << 24) | (sd_data_buf[12] << 16)
					| (sd_data_buf[11] << 8) | (sd_data_buf[10] << 0);
			tem8 = (sd_data_buf[15] & 0x0F);
			tem8 = ((sd_data_buf[14] & 0x0F) << 4)
					| ((sd_data_buf[15] & 0xF0) >> 4);
		}
	} else {
		SPI_DeInit(LPC_SPI); // DeInitialize SPI peripheral
	}
	/* Loop forever */
	while (1)
		;
}


/**
 * @}
 */

#endif /* _AUX_FUNC */

/**
 * @}
 */

/* --------------------------------- End Of File ------------------------------ */
