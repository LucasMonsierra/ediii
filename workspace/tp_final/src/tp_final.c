/*
===============================================================================
 Name        : tp_final.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

// TODO: insert other include files here
#include "config.h"
#include "auxfunction.h"
#include "lpc17xx_adc.h"
#include "lpc17xx_exti.h"
#include "lpc17xx_gpdma.h"
#include "lpc17xx_timer.h"

int main(void) {
	CONFIG_Init();
    volatile static int i = 0 ;
    while(1) { i++ ; }
    return 0 ;
}


void EINT0_IRQHandler(void) {
	// DEBERÍAMOS BAJAR ESTA BANDERA CUANDO TERMINE DE GRABAR
	// EN EL ELSE DEL HANDLER DEL TIMER0
	//EXTI_ClearEXTIFlag(EXTI_EINT0);				// Flag off
	redLedOn();									// Turn red led on
	recordAudio();								// Start record
}

void EINT1_IRQHandler(void) {
	// IGUAL QUE CON ESTA DEBERÍAMOS BAJAR ESTA BANDERA CUANDO
	// TERMINE DE SACAR TODOS LOS DATOS POR DAC
	// SIEMPRE DEJANDO QUE LAS INT DE ADC, DAC, TIM, Y DMA
	// TENGAN MÁS PRIORIDAD
	EXTI_ClearEXTIFlag(EXTI_EINT1);				// Flag off
	greenLedOn();								// Turn green led on
	transferMem2Uart();							// Transfer data to UART
	//transferMem2Dac();						// Transfer data to DAC
}

void UART0_IRQHandler(void) {

}

void TIMER0_IRQHandler(void) {
	if (TIM_GetIntStatus(LPC_TIM0, TIM_MR0_INT)) {
		TIM_ClearIntPending(LPC_TIM0, TIM_MR0_INT);	// Falg off
		ADC_StartCmd(LPC_ADC, ADC_START_NOW);		// Start conversion
	} else {
		// Recording is completed
		EXTI_ClearEXTIFlag(EXTI_EINT0);				// Flag off
		TIM_ClearIntPending(LPC_TIM0, TIM_MR1_INT);	// Falg off
		redLedOff();
	}
}

void ADC_IRQHandler(void) {
	// Conversion is success
	transferADC2Mem();							// Transfer value to Mem
}

void DMA_IRQHandler (void) {
	//Transfer to Bluethoot complete
	greenLedOff();
	// Scan interrupt pending
	for (int i = 3 ; i >= 0 ; i--) {
		if (GPDMA_IntGetStatus(GPDMA_STAT_INT, i)){
			// Check Counter terminal status
			if (GPDMA_IntGetStatus(GPDMA_STAT_INTTC, i)){
				// Clear terminate counter Interrupt pending
				GPDMA_ClearIntPending (GPDMA_STATCLR_INTTC, i);

				switch (i) {
					case 0:
						GPDMA_ChannelCmd(i, DISABLE);
						CONFIG_gpdmaConfigCh0();
						break;
					case 1:
						GPDMA_ChannelCmd(1, DISABLE);
						CONFIG_gpdmaConfigCh1();
						break;
					case 2:
						GPDMA_ChannelCmd(i, DISABLE);
						CONFIG_gpdmaConfigCh2();
						break;
					case 3:
						GPDMA_ChannelCmd(i, DISABLE);
						CONFIG_gpdmaConfigCh3();
						break;
					default:
						while (1); // An error occurred
						break;
				}
			}

			// Check Error terminal status
			if (GPDMA_IntGetStatus(GPDMA_STAT_INTERR, i)){
				// Clear error counter Interrupt pending
				GPDMA_ClearIntPending (GPDMA_STATCLR_INTERR, i);
				switch (i) {
					case 0:
						GPDMA_ChannelCmd(i, DISABLE);
						CONFIG_gpdmaConfigCh0();
						break;
					case 1:
						GPDMA_ChannelCmd(1, DISABLE);
						CONFIG_gpdmaConfigCh1();
						break;
					case 2:
						GPDMA_ChannelCmd(i, DISABLE);
						CONFIG_gpdmaConfigCh2();
						break;
					case 3:
						GPDMA_ChannelCmd(i, DISABLE);
						CONFIG_gpdmaConfigCh3();
						break;
					default:
						while (1); // An error occurred
						break;
				}
			}
		}
	}
}
